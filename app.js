const express = require("express");
const bodyParser = require("body-parser");
const tracer = require('tracer');

const moviesRoute = require("./src/routes/movies_routes");
const usersRoute = require("./src/routes/users_routes");

const app = express();
const port = process.env.PORT || 3000;

// Lambda function: () => {}
let myCallBackFunction = function () {
  console.log("De server luistert op port: " + port);
}

app.listen(port, myCallBackFunction);
app.use(bodyParser.json());

app.use("/api/movies", moviesRoute)
app.use("/api/register", usersRoute)

// exporteert de server voor toegang tot andere bestanden
module.exports = app;