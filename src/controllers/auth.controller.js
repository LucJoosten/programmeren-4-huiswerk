module.exports = {

    validate: (req, res, next) => {
        // tokens meesturen in de header voor authenticatie
        const loginCode = req.headers['logintoken']

        if (!loginCode || loginCode === null || loginCode == 0) {
            // geen toegang verlenen
            const errorObject = {
                message: "Verboden toegang!",
                code: 409
            }
            next(errorObject)
        } else {
            next()
        }
    }
}