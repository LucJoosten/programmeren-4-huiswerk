const logger = require('../config/app.config').logger;
const sqlDao = require('../database/mssql.dao');

module.exports = {

    movieHandler: (req, res, next) => {
        logger.trace('Movie handler');
        next();
    },

    getAllMovies: (req, res, next) => {
        logger.info('get /api/movies aangeroepen')

        const query = 'SELECT * FROM Movie';
        sqlDao.executeQuery(query, (err, rows) => {
            // verwerk error of result
            if(err) {
                const errorObject = {
                    message: 'Er ging iets mis in de database',
                    code: 500
                }
                next(errorObject); 
            }
            if (rows) {
                res.status(200).json(rows);   
            }
        });
    },
  

    postMovie: (req, res, next) => {
        // Create movie from data collected from the POST
        // logger.info(movie)

        const movie = req.body;
        const query = `INSERT INTO Movie (Title, Description, Year, Director) VALUES ('${movie.title}', '${movie.description}', '${movie.releaseYear}', '${movie.director}')`;
        
        sqlDao.executeQuery(query, (err, rows) => {
            // verwerk error of result
            if(err) {
                const errorObject = {
                    message: 'Er ging iets mis in de database',
                    code: 500
                }
                next(errorObject); 
            }
            if (rows) {
                res.status(200).json({Result: rows});   
            }
        });
    },

    getMovie: (req, res, next) => {

        const id = req.params.position
        const query = `SELECT * FROM Movie WHERE Movie.MovieId = ${id}`;
        sqlDao.executeQuery(query, (err, rows) => {
            // verwerk error of result
            if(err) {
                const errorObject = {
                    message: 'Er ging iets mis in de database',
                    code: 500
                }
                next(errorObject); 
            }
            if (rows) {
                res.status(200).json({Result: rows});   
            }
        });
    },

    // putMovie: (req, res, next) => {
    //     // bewaar meegegeven ID
    //     const id = req.params.position

    //     // Maak movie object van de body gegevens
    //     const movie = {
    //         title: req.body.title,
    //         description: req.body.description,
    //         releaseYear: req.body.releaseYear,
    //         director: req.body.director
    //     }

    //     // check of de film met het ID bestaat
    //     if (database.movieArray[id] != null) {
    //         database.movieArray[id] = movie;
    //         res.status(200).json(database.movieArray[id])
    //     } else {
    //         const error = {
    //             message: "Movie with ID: " + id + "not found!",
    //             code: 404
    //         }
    //         logger.error(error);
    //         next(error);
    //     }
    // },

    // deleteMovie: (req, res, next) => {
    //     // bewaar meegegeven ID
    //     const id = req.params.position;
    //     if (database.movieArray[id] != null) {
    //         database.movieArray[id] = null;
    //         res.status(200).json(`Movie with ID ${id} is deleted!`)
    //     } else {
    //         const error = {
    //             message: `Movie with ID: ${id} not found!1`,
    //             code: 404
    //         }
    //         logger.error(error);
    //         next(error);
    //     }
    // }
};