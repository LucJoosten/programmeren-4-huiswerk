const sqlDao = require('../database/mssql.dao');
const logger = require('../config/app.config').logger;


module.exports = {

  postUser: (req, res, next) => {
    // Create user from data collected from the POST method
    const user = req.body

  /* =========================================================================== */
    let str = user.email;
    let emailValidation = /\u0040/g;
    let resultEmailAddress = str.match(emailValidation);

  /* =========================================================================== */
    let postalcodeString = user.postcode;
    let postalCodeValidation = /^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i;
    let resultPostalcode = postalcodeString.match(postalCodeValidation)

  /* =========================================================================== */
    let phoneNumberString = user.telefoonNummer;
    let phoneNumberValidation = /^[0][6][0-9]{8} | [0][1-9][0-9][\s-\s][0-9]{7}$/;
    let resultPhoneNumber = phoneNumberString.match(phoneNumberValidation);

    if (resultEmailAddress) {

      if (resultPostalcode) {

        if (resultPhoneNumber) {
            logger.info(`POST Method called`);
            const query = `INSERT INTO User (FirstName, LastName, StreetAddress, PostalCode, City, DateOfBirth, PhoneNumber
              EmailAddress, Password) VALUES ('${user.voornaam}', '${user.achternaam}', '${user.adres}', '${user.postcode}', '${user.plaats}', '${user.geboorteDatum}', '${user.telefoonNummer}'
              , '${user.email}', '${user.password}')`;
        
        sqlDao.executeQuery(query, (err, rows) => {
            // verwerk error of result
            if(err) {
                const errorObject = {
                    message: 'Er ging iets mis in de database',
                    code: 500
                }
                next(errorObject); 
            }
            if (rows) {
                res.status(200).json({Result: rows});   
            }
        })
        } else {
          const error = {
            message: `User with username: ${user.naam} did not enter a valid phone number`,
            code: 406
          }
          logger.error(error);
          next(error);
        }
        
      } else {
        const error = {
          message: `User with username: ${user.naam} did not enter a valid postalcode`,
          code: 406
        }
        logger.error(error);
        next(error);
      }

    } else {
      const error = {
        message: `User with username: ${user.naam} did not enter a valid email address!`,
        code: 406
      }
      logger.error(error);
      next(error);
    }
  },

  getUser: (req, res, next) => {
    const id = req.params.position;

    if (id => 0 && id <= database.userArray.length) {
      logger.log("user found with ID: " + id)
      res.status(200).json(database.userArray[id])
    } else {
      const errorObject = {
        message: "user with userID: " + id + " was not found.",
        code: 404
      }
      logger.error(errorObject)
      next(errorObject)
    }
  }
}