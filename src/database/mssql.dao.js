const sql = require('mssql');
const config = require('../config/app.config').databaseConfig;
const logger = require('../config/app.config').logger;

module.exports = {

    executeQuery: (query, callback) => {
        sql.connect(config, err => {
            // ... error checks
            if (err) logger.error("Error connecting: ", err.toString());
            if (!err) {
                // Query
                new sql.Request().query(query, (err, result) => {
                    // ... error checks
                    if (err) {
                        logger.error("Error", err.toString());

                        // callback when there is an error, the result is null
                        callback(err, null)
                    }
                    if (result) {
                        console.dir(result);
                        // print the resultset
                        //result.recordset.forEach(item => console.log(item.number));
                        
                        // callback when no errors
                        callback(null, result)
                        
                        // close connection
                        sql.close();
                    }
                });
            }
        });

    }
}