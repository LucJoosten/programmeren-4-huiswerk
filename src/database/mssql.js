const sql = require("./node_modules/mssql");
const config = require('../config/app.config').databaseConfig;

    sql.connect(config, err => {
        // ... error checks
        if (err) console.error("Error connecting: ", err.toString());
        if (!err) {
            // Query
            new sql.Request().query("SELECT * FROM Movie", (err, result) => {
                // ... error checks
                if (err) console.log("error", err.toString());
                if (result) {
                    console.dir(result);
                    result.recordset.forEach(item => console.log(item.number));
                    sql.close();
                }
            });
        }
    });




sql.on("error", err => {
    // ... error handler
    if (err) console.log("onError: ", err);
});