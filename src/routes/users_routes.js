const express = require("express");
const router = express.Router();
const userController = require("../controllers/users.controller");
const authController = require('../controllers/auth.controller');
const logger = require('../config/app.config').logger;

router.all("*", (req, res, next) => {
  console.log("generieke afhandeling")
  next();
})

/* ***************************************************************** POST - voegt een movie toe - returned movieID */
router.post("/", authController.validate, userController.postUser);


/* ************************************************************** GET - Geeft de user met het gegeven user id - returned user informatie */
router.get("/:position", userController.getUser);


/* ********************************************************************* ERROR HANDLERS *******/
router.all("*", (req, res, next) => {
  const errorObject = {
    message: "USER Endpoint not found",
    code: 404
  }
  logger.error(errorObject)
  next(errorObject)
})

router.use(function (error, req, res, next) {
  logger.error("Error Handler aangeroepen", error.message);
  res.status(500).json(error)
});

module.exports = router;



authController.validate