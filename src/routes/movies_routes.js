const express = require("express");
const router = express.Router();
const movieController = require('../controllers/movies.controller');
const authController = require('../controllers/auth.controller');
const logger = require('../config/app.config').logger;


router.all("*", (req, res, next) => {
  logger.log("generieke afhandeling")
  next()
})

/* ******************************************************************************************** POST - voegt een movie toe - returned movieID */
router.post("/", authController.validate, movieController.postMovie);

/* ****************************************************************************** GET - Geeft alle movies die in de database zitten */
router.get('/', movieController.getAllMovies);

/* *********************************************************************** GET - Geeft de movie met het gegeven movieID - returned movie informatie */
router.get("/:position", movieController.getMovie);

/* ****************************************************************************************** PUT - Update oude movie met nieuwe ******************************/
// router.put("/:position", movieController.putMovie);

/* ********************************************************************************** DELETE - delete een movie uit de array ***************** */
// router.delete("/:position", movieController.deleteMovie);

router.all("*", (req, res, next) => {
  const errorObject = {
    message: "MOVIE Endpoint not found",
    code: 404
  }
  next(errorObject);
})

router.use((error, req, res, next) => {
  logger.error("Error Handler aangeroepen", error.message);
  res.status(500).json(error);
});

module.exports = router;