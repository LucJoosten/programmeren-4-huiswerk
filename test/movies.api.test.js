// // variables for chai which are needed to test the methods to the server
// const chai = require('chai');
// const chaiHttp = require('chai-http');
// const server = require('../app');


// chai.should();
// chai.use(chaiHttp);

// // const variable of the endpoint to use POST
// const endPoint = '/api/movies';

// // const variable of the endpoint to use GET | PUT | DELETE
// const endPointWithID_0 = '/api/movies/0';
// const endPointWithID_1 = '/api/movies/1';

// /*  ==========================       
//     =    POST METHOD TEST    =
//     ==========================
// */

// describe('Posts a movie to the server into the array', () => {
//     it('Creates a FIRST movie and posts it (POST - method)', done => {
//         chai
//             .request(server)
//             .post(endPoint)
//             .set('logintoken', '10')
//             .set('Content-Type', 'application/json')
//             .send({
//                 title: 'finding nemo',
//                 description: 'vissen film',
//                 releaseYear: 2016,
//                 director: 'robin'
//             })
//             .end((err, res) => {
//                 res.should.have.status(200);
//                 res.body.should.be.a('object');
//                 res.body.should.have.property('title').equals('finding nemo');
//                 res.body.should.have.property('description').equals('vissen film');
//                 res.body.should.have.property('releaseYear').equals(2016);
//                 res.body.should.have.property('director').that.is.a('string');
//                 res.body.should.not.have.property('password');
//                 done();
//             });
//     });

//     it('Creates a SECOND movie and posts it (POST - method)', done => {
//         chai
//             .request(server)
//             .post(endPoint)
//             .set('logintoken', '10')
//             .set('Content-Type', 'application/json')
//             .send({
//                 title: 'King Kong',
//                 description: 'Apen Film',
//                 releaseYear: 2018,
//                 director: 'Stefan'
//             })
//             .end((err, res) => {
//                 res.should.have.status(200);
//                 res.body.should.be.a('object');
//                 res.body.should.have.property('title').equals('King Kong');
//                 res.body.should.have.property('description').equals('Apen Film');
//                 res.body.should.have.property('releaseYear').equals(2018);
//                 res.body.should.have.property('director').that.is.a('string');
//                 res.body.should.not.have.property('password');
//                 done();
//             });
//     });

//     it("should return an error with code: 404", done => {
//         chai
//             .request(server)
//             .post("/random")
//             .end((err, res) => {

//                 // Because the path is not right, the status should
//                 // return an error with status code 404
//                 res.status.should.equal(404);
//                 done();
//             });
//     });
// });


// // /*  ==========================       
// //     =    GET METHOD TEST     =
// //     ==========================
// // */

// // describe('Gets a movie from the server by ID', () => {

// //     it('Returns a movie by ID (GET - method)', done => {
// //         chai
// //             .request(server)
// //             .get(endPointWithID_1)
// //             .end((err, res) => {
// //                 // The response status should be equal to 200
// //                 res.status.should.equal(200);

// //                 const movieTitle = 'King Kong';
// //                 const movieDescription = 'Apen Film';
// //                 const movieReleaseYear = 2018;
// //                 const movieDirector = 'Stefan';


// //                 // the body should be equal to the posted object on place [1]
// //                 res.body.title.should.equal(movieTitle);
// //                 res.body.description.should.equal(movieDescription);
// //                 res.body.releaseYear.should.equal(movieReleaseYear);
// //                 res.body.director.should.equal(movieDirector);
// //                 // console.log(res.body)
// //                 // console.log(getMovieObj)
// //                 done();
// //             });
// //     });

// //     it("should return an error with code: 404", done => {
// //         chai
// //             .request(server)
// //             .get("/random")
// //             .end((err, res) => {

// //                 // Because the path is not right, the status should
// //                 // return an error with status code 404
// //                 res.status.should.equal(404);
// //                 done();
// //             });
// //     });
// // });


// // /*  ==========================       
// //     =    PUT METHOD TEST     =
// //     ==========================
// // */
// // describe('Updates a movie from the server by ID', () => {

// //     it('Updates a movie by ID (PUT - method)', done => {
// //         chai
// //             .request(server)
// //             .get(endPointWithID_0)
// //             .end((err, res) => {
// //                 res.status.should.equal(200);
// //                 chai.request(server)
// //                     .put(endPointWithID_0)
// //                     .send({
// //                         'title': 'Finding Dory'
// //                     })
// //                     .send({
// //                         'description': 'Dory Film'
// //                     })
// //                     .send({
// //                         'releaseYear': 2020
// //                     })
// //                     .send({
// //                         'director': 'Anjuli jhakry'
// //                     })
// //                     .end((err, response) => {
// //                         response.status.should.equal(200).json(`Updated movie with movieID ${id}`);
// //                         response.should.be.json;
// //                         response.body.should.be.a('object');
// //                         response.body.should.have.property('title');
// //                         response.body.should.have.property('description');
// //                         response.body.should.have.property('releaseYear');
// //                         response.body.should.have.property('director');
// //                         response.body.title.should.equal('Finding Dory');
// //                         response.body.description.should.equal('Dory Film');
// //                         response.body.releaseYear.should.equal(2020);
// //                         response.body.director.should.equal('Anjuli jhakry');
// //                         done();
// //                     });


// //             });
// //     });

// //     it("should return an error with code: 404", done => {
// //         chai
// //             .request(server)
// //             .get("/random")
// //             .end((err, res) => {

// //                 // Because the path is not right, the status should
// //                 // return an error with status code 404
// //                 res.status.should.equal(404);
// //                 done();
// //             });
// //     });
// // });


// // /*  ==========================       
// //     =    DELETE METHOD TEST  =
// //     ==========================
// // */
// // describe('Deletes a movie from the server by ID', () => {

// //     it('Deletes a movie by ID (PUT - method)', done => {
// //         chai
// //             .request(server)
// //             .get(endPointWithID_0)
// //             .end((err, res) => {
// //                 res.status.should.equal(200);
// //                 chai.request(server)
// //                     .delete(endPointWithID_0)
// //                     .end((err, response) => {
// //                         response.status(200).json('Movie with ID: ' + id + ' is deleted')
// //                         response.should.be.json;
// //                         response.body.should.not.have.property('title');
// //                         response.body.should.not.have.property('description');
// //                         response.body.should.not.have.property('releaseYear');
// //                         response.body.should.not.have.property('director');
// //                         done();
// //                     });


// //             });
// //     });

// //     it("should return an error with code: 404", done => {
// //         chai
// //             .request(server)
// //             .get("/random")
// //             .end((err, res) => {

// //                 // Because the path is not right, the status should
// //                 // return an error with status code 404
// //                 res.status.should.equal(404);
// //                 done();
// //             });
// //     });
// // });