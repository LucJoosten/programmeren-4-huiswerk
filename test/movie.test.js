const database = require('../src/database/database');
const assert = require('assert');


describe("Add Movie", () => {
    it("Should save a movie in the MOVIE array", (done) => {
        // zet een movie in de database
        const movie = {
            title: "Finding Nemo",
            description: "Vissen film",
            releaseYear: "2016",
            director: "Robin"
        }

        database.movieArray.push(movie)
        
        // verwachting: als het gelukt is, dan staat de movie op index[0]
        assert(database.movieArray[0].title === "Finding Nemo")
        assert(database.movieArray[0].releaseYear === "2016")
        assert(database.movieArray[0].director === "Robin")
        
        done()
    })
})

