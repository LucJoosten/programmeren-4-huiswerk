// variables for chai which are needed to test the methods to the server
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');


chai.should();
chai.use(chaiHttp);

// const variable of the endpoint to use POST
const endPoint = '/api/register';

// const variable of the endpoint to use GET | PUT | DELETE
const endPointWithID_0 = '/api/register/0';
const endPointWithID_1 = '/api/register/1';


/*  ==========================       
    =    POST METHOD TEST    =
    ==========================
*/

describe('Posts a user to the server into the array', () => {
    it('Creates a FIRST user and posts it (POST - method)', done => {
        chai
            .request(server)
            .post(endPoint)
            .set('logintoken', '10')
            .set('Content-Type', 'application/json')
            .send({
                naam: "Luc Joosten",
                adres: "Buys Ballotstraat 17",
                postcode: "5756BH",
                plaats: "Vlierden",
                geboorteDatum: "14-12-1999",
                telefoonNummer: "0636542018",
                email: "lucjoosten1234@outlook.com",
                password: "Avans1234!"
            })
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('naam').equals('Luc Joosten');
                res.body.should.have.property('adres').equals('Buys Ballotstraat 17');
                res.body.should.have.property('postcode').that.is.a('string');
                res.body.should.have.property('plaats').equals('Vlierden');
                res.body.should.have.property('geboorteDatum').that.is.a('string');
                res.body.should.have.property('telefoonNummer').equals('0636542018');
                res.body.should.have.property('email').equals('lucjoosten1234@outlook.com');
                res.body.should.have.property('password').that.is.a('string');
                done();
            });
    });

    it("should return an error with code: 404", done => {
        chai
            .request(server)
            .post("/registet")
            .end((err, res) => {

                // Because the path is not right, the status should
                // return an error with status code 404
                res.status.should.equal(404);
                done();
            });
    });
});


/*  ==========================       
    =    GET METHOD TEST     =
    ==========================
*/

describe('Gets a user from the server by ID', () => {

    it('Returns a user by ID (GET - method)', done => {
        chai
            .request(server)
            .get(endPointWithID_0)
            .end((err, res) => {
                // The response status should be equal to 200
                res.status.should.equal(200);

                const userName = 'Luc Joosten';
                const userAddres = 'Buys Ballotstraat 17';
                const userPostalcode = '5756BH';
                const userLocation = 'Vlierden';
                const userBirth = '14-12-1999';
                const userPhoneNumber = '0636542018';
                const userEmail = 'lucjoosten1234@outlook.com';
                const userPassword = 'Avans1234!';


                // the body should be equal to the posted object on place [0]
                res.body.naam.should.equal(userName);
                res.body.adres.should.equal(userAddres);
                res.body.postcode.should.equal(userPostalcode);
                res.body.plaats.should.equal(userLocation);
                res.body.geboorteDatum.should.equal(userBirth);
                res.body.telefoonNummer.should.equal(userPhoneNumber);
                res.body.email.should.equal(userEmail);
                res.body.password.should.equal(userPassword);
                
                done();
            });
    });

    it("should return an error with code: 404", done => {
        chai
            .request(server)
            .get("/register/2")
            .end((err, res) => {

                // Because the path is not right, the status should
                // return an error with status code 404
                res.status.should.equal(404);
                done();
            });
    });
});