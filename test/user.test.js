const database = require('../src/database/database');
const assert = require('assert');

describe("Add User", () => {
    it("Should save a user in the USER array", (done) => {
        // zet een movie in de database
        const user = {
            id: "1",
            naam: "Luc Joosten",
            adres: "Buys Ballotstraat 17",
            postcode: "5756BH",
            plaats: "Vlierden",
            geboorteDatum: "14-12-1999",
            telefoonNummer: "0636542018",
            email: "lucjoosten1234@outlook.com",
            password: "Avans1234!"
        }

        database.userArray.push(user)
        
        // verwachting: als het gelukt is, dan staat de movie op index[0]
        assert(database.userArray[0].naam === "Luc Joosten")
        assert(database.userArray[0].id === "1")
        assert(database.userArray[0].email === "lucjoosten1234@outlook.com")
        assert(database.userArray[0].password === "Avans1234!")

        done()
    })
})